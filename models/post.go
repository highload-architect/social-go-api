package models

import (
	"github.com/google/uuid"
	"time"
)

type Post struct {
	ID           uuid.UUID `db:"id" json:"id"`
	Text         string    `db:"text" json:"text"`
	AuthorUserID string    `db:"author_user_id" json:"author_user_id"`
	CreatedAts   time.Time `db:"created_at" json:"created_at"`
}
