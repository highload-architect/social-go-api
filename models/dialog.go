package models

import (
	"time"
)

type Dialog struct {
	ID         int       `json:"id" db:"id"`
	FromUserID string    `json:"from_user_id" db:"from_user_id"`
	ToUserID   string    `json:"to_user_id" db:"to_user_id"`
	Text       string    `json:"text" db:"text"`
	CreatedAt  time.Time `db:"created_at" json:"created_at"`
}
