package models

import (
	"github.com/google/uuid"
	"time"
)

type User struct {
	ID         uuid.UUID `db:"id" json:"id"`
	Email      string    `db:"email" json:"email"`
	FirstName  string    `db:"first_name" json:"first_name"`
	SecondName string    `db:"second_name" json:"second_name"`
	Birthdate  string    `db:"birthdate" json:"birthdate"`
	Biography  string    `db:"biography" json:"biography"`
	City       string    `db:"city" json:"city"`
	Password   string    `db:"password" json:"password,omitempty"`
	CreatedAt  time.Time `db:"created_at" json:"created_at"`
}
