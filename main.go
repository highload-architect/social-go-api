package main

import (
	"fmt"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"log"
	"social-go-api/database"
	"social-go-api/routes"
	"social-go-api/utils"
)

func main() {
	// Инициализация переменных
	utils.LoadEnvVariables()

	// Инициализация подключения к базе данных
	_, err := database.InitPostgres()
	if err != nil {
		// Обработка ошибки и завершение приложения в случае неудачи
		panic("Failed to connect to the database: " + err.Error())
	} else {
		fmt.Println("Successfully connected to database")
	}
	// Инициализация Gin
	r := gin.Default()

	// Включаем CORS и разрешаем заголовок Authorization
	r.Use(cors.New(cors.Config{
		AllowOrigins:     []string{"*"}, // или укажите разрешенные домены
		AllowMethods:     []string{"GET", "POST", "PUT", "DELETE"},
		AllowHeaders:     []string{"Authorization", "Content-Type"},
		AllowCredentials: true,
	}))

	// Подключение маршрутов
	routes.SetupRoutes(r)

	// Запуск сервера
	if err := r.Run(":8080"); err != nil {
		log.Fatal(err)
	}
}
