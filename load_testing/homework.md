## ДЗ: Производительность индексов

- Данные сгенерированы с помощью бибиотеки faker    
[Файл для генерации - users.go](https://gitlab.com/highload-architect/social-go-api/-/blob/main/database/faker/users.go?ref_type=heads)

- Метод для тестирования:  
`/user/search?first_name=:any1&second_name=:any2`

## Отчет 
 
### Графики

#### График latency до индекса

Рандомный запрос  
![График latency до индекса](https://gitlab.com/highload-architect/social-go-api/-/raw/main/load_testing/graphs/latency_before_index.png)

Фиксированный запрос  
![График latency до индекса](https://gitlab.com/highload-architect/social-go-api/-/raw/main/load_testing/graphs/latency_before_index_same_search.png?ref_type=heads)

#### График throughput до индекса

Рандомный запрос  
![График throughput до индекса](https://gitlab.com/highload-architect/social-go-api/-/raw/main/load_testing/graphs/throughput_before_index.png?ref_type=heads)

Фиксированный запрос  
![График throughput до индекса](https://gitlab.com/highload-architect/social-go-api/-/raw/main/load_testing/graphs/throughput_before_index_same_search.png?ref_type=heads)

#### График latency b-tree

Рандомный запрос  
![График latency b-tree](https://gitlab.com/highload-architect/social-go-api/-/raw/main/load_testing/graphs/latency_b_tree.png?ref_type=heads)

Фиксированный запрос  
![График latency b-tree](https://gitlab.com/highload-architect/social-go-api/-/raw/main/load_testing/graphs/latency_gin_same_search.png?ref_type=heads)

#### График throughput b-tree

Рандомный запрос  
![График throughput b-tree](https://gitlab.com/highload-architect/social-go-api/-/raw/main/load_testing/graphs/throughput_b_tree.png?ref_type=heads)

Фиксированный запрос  
![График throughput b-tree](https://gitlab.com/highload-architect/social-go-api/-/raw/main/load_testing/graphs/throughput_b_tree_same_search.png?ref_type=heads)

#### График latency GIN

Рандомный запрос  
![График latency GIN](https://gitlab.com/highload-architect/social-go-api/-/raw/main/load_testing/graphs/latency_gin.png?ref_type=heads)

Фиксированный запрос  
![График latency GIN](https://gitlab.com/highload-architect/social-go-api/-/raw/main/load_testing/graphs/latency_gin_same_search.png?ref_type=heads)

#### График throughput GIN

Рандомный запрос  
![График throughput GIN](https://gitlab.com/highload-architect/social-go-api/-/raw/main/load_testing/graphs/throughput_gin.png?ref_type=heads)

Фиксированный запрос  
![График throughput GIN](https://gitlab.com/highload-architect/social-go-api/-/raw/main/load_testing/graphs/throughput_gin_same_search.png?ref_type=heads)

### Запросы добавления индексов

```SQL
-- Создание GIN индекса
CREATE EXTENSION IF NOT EXISTS pg_trgm;
CREATE INDEX idx_users_name_prefix ON users USING GIN (first_name gin_trgm_ops, second_name gin_trgm_ops);
```

```SQL
-- Создание B-tree индекса
CREATE INDEX idx_users_name_prefix ON users (first_name varchar_pattern_ops, second_name varchar_pattern_ops);
```

```SQL
EXPLAIN ANALYZE
SELECT *
FROM users
WHERE first_name LIKE 'A%'
AND second_name LIKE 'B%';
```

### Explain запросов до индекса

```bash
 Gather  (cost=1000.00..38305.87 rows=9328 width=182) (actual time=0.435..81.425 rows=9235 loops=1)
   Workers Planned: 2
   Workers Launched: 2
   ->  Parallel Seq Scan on users  (cost=0.00..36373.07 rows=3887 width=182) (actual time=0.079..73.163 rows=3078 loops=3)
         Filter: (((first_name)::text ~~ 'A%'::text) AND ((second_name)::text ~~ 'B%'::text))
         Rows Removed by Filter: 331432
 Planning Time: 0.153 ms
 Execution Time: 81.905 ms
```

### Explain запросов b-tee

```bash
 Bitmap Heap Scan on users  (cost=2882.96..23534.48 rows=9328 width=182) (actual time=12.107..26.164 rows=9235 loops=1)
   Filter: (((first_name)::text ~~ 'A%'::text) AND ((second_name)::text ~~ 'B%'::text))
   Heap Blocks: exact=7838
   ->  Bitmap Index Scan on idx_users_name_prefix  (cost=0.00..2880.63 rows=9942 width=0) (actual time=10.472..10.473 rows=9235 loops=1)
         Index Cond: (((first_name)::text ~>=~ 'A'::text) AND ((first_name)::text ~<~ 'B'::text) AND ((second_name)::text ~>=~ 'B'::text) AND ((second_name)::text ~<~ 'C'::text))
 Planning Time: 0.111 ms
 Execution Time: 26.583 ms
```

### Explain запросов GIN

```bash
 Bitmap Heap Scan on users  (cost=91.36..19987.48 rows=9328 width=182) (actual time=10.936..21.270 rows=9235 loops=1)
   Recheck Cond: (((first_name)::text ~~ 'A%'::text) AND ((second_name)::text ~~ 'B%'::text))
   Rows Removed by Index Recheck: 30
   Heap Blocks: exact=7862
   ->  Bitmap Index Scan on idx_users_name_prefix  (cost=0.00..89.03 rows=9328 width=0) (actual time=9.426..9.427 rows=9265 loops=1)
         Index Cond: (((first_name)::text ~~ 'A%'::text) AND ((second_name)::text ~~ 'B%'::text))
 Planning Time: 0.110 ms
 Execution Time: 21.649 ms
```

### Вывод


#### Без индекса:

    План выполнения показывает, что происходит параллельное последовательное сканирование таблицы users.
    Общая стоимость выполнения запросов (cost=1000.00..38305.87) высока из-за необходимости последовательного сканирования всей таблицы для поиска совпадений.
    Время выполнения запроса (Execution Time) составляет около 81-97 ms.

#### B-tree индекс:

    Время выполнения запроса (Execution Time) составляет около 21-27 ms, что быстрее, чем при отсутствии индекса.
    Общая стоимость выполнения запроса (cost=2882.96..23534.48) значительно ниже.

#### GIN индекс:

    Время выполнения запроса (Execution Time) составляет около 20-21 ms, что несколько быстрее, чем при использовании B-tree индекса.
    Общая стоимость выполнения запроса (cost=91.36..19987.48) существенно ниже, чем у B-tree индекса.

### Вывод:

    GIN индекс обеспечивает наилучшее время выполнения (около 20-21 ms), что быстрее, чем при использовании B-tree индекса (около 21-27 ms).
    GIN индекс также имеет значительно более низкую общую стоимость выполнения запроса (cost=91.36..19987.48), чем у B-tree индекса (cost=2882.96..23534.48).

- Таким образом, можно сделать вывод, что GIN индекс является наиболее эффективным выбором для данной задачи, так как он обеспечивает лучшее время выполнения запроса и более низкую общую стоимость.
