package middleware

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"social-go-api/utils"
	"strings"
)

// AuthMiddleware проверяет наличие и валидность токена в заголовке запроса
func AuthMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		authHeader := c.GetHeader("Authorization")

		if authHeader == "" {
			c.JSON(http.StatusUnauthorized, gin.H{"error": "Authorization header is missing"})
			c.Abort()
			return
		}

		// Получаем токен из заголовка
		tokenString := strings.Split(authHeader, " ")[1]
		if tokenString == "" {
			c.JSON(http.StatusUnauthorized, gin.H{"error": "Invalid token"})
			c.Abort()
			return
		}

		// Проверяем токен
		user, err := utils.ValidateToken(tokenString)
		if err != nil {
			c.JSON(http.StatusUnauthorized, gin.H{"error": "Invalid token"})
			c.Abort()
			return
		}

		// Передаем пользователя в контекст для последующего использования
		c.Set("user", user)

		// Продолжаем выполнение запроса
		c.Next()
	}
}
