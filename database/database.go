package database

import (
	"fmt"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"log"
)

var db *sqlx.DB

// InitPostgres инициализирует подключение к базе данных PostgreSQL
func InitPostgres() (*sqlx.DB, error) {
	config := GetPostgresConfig()
	connectionString := fmt.Sprintf("host=%s port=%d dbname=%s user=%s password=%s sslmode=%s",
		config.Host, config.Port, config.DBName, config.User, config.Password, config.SSLMode)

	database, err := sqlx.Connect("postgres", connectionString)
	if err != nil {
		log.Fatal("Failed to connect to the database:", err)
	}

	db = database

	return db, err
}

// GetDB возвращает активное подключение к базе данных
func GetDB() *sqlx.DB {
	return db
}

// CloseDB закрывает соединение с базой данных
func CloseDB() {
	err := db.Close()
	if err != nil {
		log.Println("Error closing database connection:", err)
	}
}

// PingDB проверяет подключение к базе данных
func PingDB() error {
	return db.Ping()
}
