package main

import (
	"database/sql"
	"fmt"
	"github.com/bxcodec/faker/v3"
	"github.com/joho/godotenv"
	_ "github.com/lib/pq"
	"golang.org/x/crypto/bcrypt"
	"log"
)

type User struct {
	ID         string `faker:"uuid_hyphenated"`
	Email      string `faker:"email"`
	FirstName  string `faker:"first_name"`
	SecondName string `faker:"last_name"`
	Birthdate  string `faker:"date"`
	Biography  string `faker:"sentence"`
	City       string `faker:"word"`
	Password   string `faker:"password"`
}

func main() {
	// Load environment variables from .env file
	err := godotenv.Load(".env")
	if err != nil {
		log.Fatalf("Error loading .env file: %v", err)
	}

	// Connect to the PostgreSQL database
	dbinfo := fmt.Sprintf("host=localhost port=5432 dbname=social user=postgres password=postgres sslmode=disable")
	//os.Getenv("PG_HOST"), os.Getenv("PG_PORT"), os.Getenv("PG_NAME"), os.Getenv("PG_USER"), os.Getenv("PG_PASSWORD"))
	db, err := sql.Open("postgres", dbinfo)
	if err != nil {
		log.Fatal(err)
	}
	defer db.Close()

	// Verify that we can connect to the database
	err = db.Ping()
	if err != nil {
		log.Fatal(err)
	}

	// Prepare the SQL statement for inserting data into the users table
	stmt, err := db.Prepare("INSERT INTO users(id, email, first_name, second_name, birthdate, biography, city, password) VALUES($1, $2, $3, $4, $5, $6, $7, $8)")
	if err != nil {
		log.Fatal(err)
	}
	defer stmt.Close()

	// Generate and insert fake user data into the users table
	//faker.SetLocale("ru_RU")
	//LangRUS = langRuneBoundary{1025, 1105, nil}
	password := "12345678"
	hashedPassword, err := hashPassword(password)
	if err != nil {
		log.Fatal(err)
		return
	}

	for i := 0; i < 100000; i++ {
		user := User{}
		err := faker.FakeData(&user)
		user.Password = hashedPassword
		user.FirstName = "Konstantin"
		user.SecondName = "Konstantinovskiy"

		if err != nil {
			log.Fatal(err)
		}
		// Execute the SQL statement with the fake user data
		_, err = stmt.Exec(user.ID, user.Email, user.FirstName, user.SecondName, user.Birthdate, user.Biography, user.City, user.Password)
		if err != nil {
			log.Fatal(err)
		}
		fmt.Println("Inserted user:", user.FirstName, user.SecondName)
	}

	fmt.Println("Data inserted successfully!")
}

// hashPassword hashes the given password using bcrypt
func hashPassword(password string) (string, error) {
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		return "", err
	}
	return string(hashedPassword), nil
}
