-- Создание базы данных "social"
CREATE DATABASE social;

-- Использование базы данных "social"
\c social;

-- Создание таблицы пользователей
CREATE TABLE IF NOT EXISTS users
(
    id          UUID PRIMARY KEY,
    email       VARCHAR(255) UNIQUE,
    first_name  VARCHAR(255),
    second_name VARCHAR(255),
    birthdate   DATE,
    biography   TEXT,
    city        VARCHAR(255),
    password    VARCHAR(255),
    created_at     TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

-- Создание таблицы постов
CREATE TABLE IF NOT EXISTS posts
(
    id             UUID PRIMARY KEY,
    text           TEXT,
    author_user_id UUID REFERENCES users (id),
    created_at     TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

-- Создание таблицы друзей
CREATE TABLE IF NOT EXISTS friends
(
    user_id   UUID REFERENCES users (id),
    friend_id UUID REFERENCES users (id),
    PRIMARY KEY (user_id, friend_id),
    created_at     TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);

-- Создание таблицы диалогов
CREATE TABLE IF NOT EXISTS dialogs
(
    id           SERIAL PRIMARY KEY,
    from_user_id UUID REFERENCES users (id),
    to_user_id   UUID REFERENCES users (id),
    text         TEXT,
    created_at     TIMESTAMP DEFAULT CURRENT_TIMESTAMP
);


-- Создание индексов для улучшения производительности
-- CREATE INDEX IF NOT EXISTS idx_user_first_name ON users (first_name);
-- CREATE INDEX IF NOT EXISTS idx_user_second_name ON users (second_name);
-- CREATE INDEX IF NOT EXISTS idx_post_author_user_id ON posts (author_user_id);
-- CREATE INDEX IF NOT EXISTS idx_friend_user_id ON friends (user_id);
-- CREATE INDEX IF NOT EXISTS idx_dialog_from_user_id ON dialogs (from_user_id);
-- CREATE INDEX IF NOT EXISTS idx_dialog_to_user_id ON dialogs (to_user_id);

-- Создание GIN индекса для поиска пользователей по префиксу имени и фамилии
CREATE EXTENSION IF NOT EXISTS pg_trgm;
CREATE INDEX idx_users_name_prefix ON users USING GIN (first_name gin_trgm_ops, second_name gin_trgm_ops);
-- Удаление расширения pg_trgm
-- DROP EXTENSION IF EXISTS pg_trgm;
-- -- Создание B-tree индекса для поиска пользователей по префиксу имени и фамилии
-- CREATE INDEX idx_users_name_prefix ON users (first_name varchar_pattern_ops, second_name varchar_pattern_ops);
-- -- Удаление индекса
-- DROP INDEX IF EXISTS idx_users_name_prefix;
--
-- EXPLAIN ANALYZE
-- SELECT *
-- FROM users
-- WHERE first_name LIKE 'A%'
-- AND second_name LIKE 'B%';
