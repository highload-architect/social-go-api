package database

import (
	"os"
	"strconv"
)

// PostgresConfig содержит конфигурацию для подключения к PostgreSQL
type PostgresConfig struct {
	Host     string
	Port     int
	DBName   string
	User     string
	Password string
	SSLMode  string
}

// GetPostgresConfig возвращает конфигурацию для подключения к PostgreSQL
func GetPostgresConfig() PostgresConfig {
	port, _ := strconv.Atoi(os.Getenv("PG_PORT"))
	return PostgresConfig{
		Host:     os.Getenv("PG_HOST"),
		Port:     port,
		DBName:   os.Getenv("PG_DBNAME"),
		User:     os.Getenv("PG_USER"),
		Password: os.Getenv("PG_PASSWORD"),
		SSLMode:  os.Getenv("PG_SSLMODE"),
	}
}
