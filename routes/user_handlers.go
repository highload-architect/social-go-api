package routes

import (
	"database/sql"
	"errors"
	"github.com/gin-gonic/gin"
	"net/http"
	"social-go-api/database"
	"social-go-api/models"
)

func getUserHandler(c *gin.Context) {
	// Валидация входных параметров
	userID := c.Param("id")
	if userID == "" {
		c.JSON(http.StatusBadRequest, gin.H{"error": "User ID is required"})
		return
	}

	// Запрос к базе данных для получения пользователя
	db := database.GetDB()
	query := "SELECT id, email, first_name, second_name, birthdate, biography, city FROM users WHERE id = $1"
	var user models.User
	row := db.Get(&user, query, userID)
	if errors.Is(row, sql.ErrNoRows) {
		c.JSON(http.StatusNotFound, gin.H{"error": "User not found"})
		return
	}

	// Возвращаем данные пользователя в формате JSON
	c.JSON(http.StatusOK, user)
}

func searchUsersHandler(c *gin.Context) {
	// Получаем параметры запроса
	firstName := c.Query("first_name")
	secondName := c.Query("second_name")
	email := c.Query("email")

	// Выполняем поиск пользователей в базе данных
	db := database.GetDB()
	users := make([]models.User, 0)
	query := `SELECT id, email, first_name, second_name, birthdate, biography, city FROM users 
			WHERE first_name LIKE $1 AND second_name LIKE $2 AND email like $3 ORDER BY id LIMIT 10000`
	err := db.Select(&users, query, firstName+"%", secondName+"%", email+"%")
	if err != nil {
		// Если произошла ошибка выполнения запроса, проверяем, является ли она ErrNoRows
		if errors.Is(err, sql.ErrNoRows) {
			// Если запрос не вернул результатов, отправляем пустой массив пользователей
			c.JSON(http.StatusOK, []models.User{})
			return
		}
		// Если произошла другая ошибка, возвращаем ошибку сервера
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to execute query"})
		return
	}

	// Отправляем результат клиенту
	c.JSON(http.StatusOK, users)
}
