package routes

import (
	"github.com/gin-gonic/gin"
	"social-go-api/middleware"
)

func SetupRoutes(router *gin.Engine) {
	// Группа маршрутов для аутентификации
	authGroup := router.Group("/auth")
	{
		authGroup.POST("/login", loginHandler)
		authGroup.POST("/register", registerHandler)

	}

	// Группа маршрутов для пользователей
	userGroup := router.Group("/user")
	{
		userGroup.GET("/:id", middleware.AuthMiddleware(), getUserHandler)
		userGroup.GET("/search", middleware.AuthMiddleware(), searchUsersHandler)
	}

	// Группа маршрутов для друзей
	friendGroup := router.Group("/friend")
	{
		friendGroup.POST("/:user_id", middleware.AuthMiddleware(), setFriendHandler)
		friendGroup.DELETE("/:user_id", middleware.AuthMiddleware(), deleteFriendHandler)
	}

	// Группа маршрутов для постов
	postGroup := router.Group("/post")
	{
		postGroup.POST("/", middleware.AuthMiddleware(), createPostHandler)
		postGroup.PUT("/:id", middleware.AuthMiddleware(), updatePostHandler)
		postGroup.DELETE("/:id", middleware.AuthMiddleware(), deletePostHandler)
		postGroup.GET("/:id", middleware.AuthMiddleware(), getPostHandler)
		postGroup.GET("/feed", middleware.AuthMiddleware(), getFeedHandler)
	}

	// Группа маршрутов для диалогов
	dialogGroup := router.Group("/dialog")
	{
		dialogGroup.POST("/:user_id", middleware.AuthMiddleware(), sendMessageHandler)
		dialogGroup.GET("/:user_id", middleware.AuthMiddleware(), getDialogHandler)
	}
}
