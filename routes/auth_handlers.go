package routes

import (
	"database/sql"
	"errors"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"golang.org/x/crypto/bcrypt"
	"net/http"
	"social-go-api/database"
	"social-go-api/models"
	"social-go-api/utils"
)

// loginHandler handles the login endpoint
func loginHandler(c *gin.Context) {
	// Получаем идентификатор пользователя и пароль из запроса
	var loginReq struct {
		Email    string `json:"email"`
		Password string `json:"password"`
	}

	if err := c.ShouldBindJSON(&loginReq); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid request body"})
		return
	}

	// Подключение к базе данных
	db := database.GetDB()

	// Поиск пользователя в базе данных по идентификатору
	var user models.User
	row := db.Get(&user, "SELECT * FROM users WHERE email = $1", loginReq.Email)
	if errors.Is(row, sql.ErrNoRows) {
		c.JSON(http.StatusNotFound, gin.H{"error": "User not found"})
		return
	}

	// Расшифровка пароля и проверка
	if !checkPassword(loginReq.Password, user.Password) {
		c.JSON(http.StatusUnauthorized, gin.H{"error": "Invalid credentials"})
		return
	}

	// Генерация токена
	token, err := utils.GenerateToken(user)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Error while generating token", "err": err})
		return
	}

	// Отправка токена в ответе
	c.JSON(http.StatusOK, gin.H{"token": token, "userId": user.ID, "email": user.Email})
}

func registerHandler(c *gin.Context) {
	// Валидация входных параметров
	var requestBody struct {
		Email      string `json:"email" binding:"required"`
		Password   string `json:"password" binding:"required"`
		FirstName  string `json:"first_name" binding:"required"`
		SecondName string `json:"second_name" binding:"required"`
		Birthdate  string `json:"birthdate" binding:"-"`
		Biography  string `json:"biography" binding:"-"`
		City       string `json:"city" binding:"-"`
	}

	if err := c.ShouldBindJSON(&requestBody); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	// Проверка, что email уже существует в базе данных
	db := database.GetDB()
	var existingUser models.User
	err := db.Get(&existingUser, "SELECT * FROM users WHERE email = $1", requestBody.Email)
	if err == nil {
		// Пользователь с таким email уже существует
		c.JSON(http.StatusConflict, gin.H{"error": "User with this email already exists"})
		return
	}

	hashedPassword, err := hashPassword(requestBody.Password)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to hash password"})
		return
	}

	user := models.User{
		ID:         uuid.New(),
		Email:      requestBody.Email,
		Password:   hashedPassword,
		FirstName:  requestBody.FirstName,
		SecondName: requestBody.SecondName,
		Birthdate:  requestBody.Birthdate,
		Biography:  requestBody.Biography,
		City:       requestBody.City,
	}

	// Perform database insertion
	queryString := `INSERT INTO users (id, email, first_name, second_name, birthdate, biography, city, password)
					VALUES ($1, $2, $3, $4, $5, $6, $7, $8)`
	result := db.MustExec(queryString, user.ID, user.Email, user.FirstName, user.SecondName, user.Birthdate, user.Biography, user.City, user.Password)
	if result == nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to register user", "err": err})
		return
	}

	c.JSON(http.StatusOK, gin.H{"user_id": user.ID})
}

// hashPassword hashes the given password using bcrypt
func hashPassword(password string) (string, error) {
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	if err != nil {
		return "", err
	}
	return string(hashedPassword), nil
}

// checkPassword сравнивает введенный пароль с хэшированным паролем пользователя
func checkPassword(inputPassword, hashedPassword string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hashedPassword), []byte(inputPassword))
	return err == nil
}
