package routes

import (
	"database/sql"
	"errors"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"net/http"
	"social-go-api/database"
	"social-go-api/models"
	"strconv"
)

func createPostHandler(c *gin.Context) {
	var requestBody struct {
		Text string `json:"text" binding:"required"`
	}
	if err := c.ShouldBindJSON(&requestBody); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		return
	}

	// Получаем текущего пользователя из контекста
	currentUserID, exists := c.Get("user")
	if !exists {
		c.JSON(http.StatusUnauthorized, gin.H{"error": "User not authenticated"})
		return
	}
	userIDString, ok := currentUserID.(string)
	if !ok {
		// Обработка случая, когда userID не является строкой
		panic("userID is not a string")
	}

	db := database.GetDB()

	newPost := models.Post{
		ID:           uuid.New(),
		Text:         requestBody.Text,
		AuthorUserID: userIDString,
	}

	queryString := "INSERT INTO posts (id, text, author_user_id) VALUES ($1, $2, $3)"
	result := db.MustExec(queryString, newPost.ID, newPost.Text, newPost.AuthorUserID)

	if result == nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to save post user"})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"message": "Пост успешно создан",
		"data":    newPost,
	})
}

func updatePostHandler(c *gin.Context) {
	// Получаем идентификатор поста из параметров маршрута
	postID := c.Param("id")
	if postID == "" {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Post ID is required"})
		return
	}

	var requestBody struct {
		Text string `json:"text" binding:"required"`
	}

	if err := c.ShouldBindJSON(&requestBody); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": err.Error(),
		})
		return
	}

	// Получаем текущего пользователя из контекста
	currentUserID, exists := c.Get("user")
	if !exists {
		c.JSON(http.StatusUnauthorized, gin.H{"error": "User not authenticated"})
		return
	}
	userIDString, ok := currentUserID.(string)
	if !ok {
		// Обработка случая, когда userID не является строкой
		panic("userID is not a string")
	}

	// Проверяем, является ли пользователь автором поста
	if !IsPostAuthor(postID, userIDString) {
		c.JSON(http.StatusForbidden, gin.H{"error": "User is not the author of the post"})
		return
	}

	// Обновляем пост в базе данных
	db := database.GetDB()
	query := "UPDATE posts SET text = $1 WHERE id = $2"
	_, err := db.Exec(query, requestBody.Text, postID)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to update post", "err": err})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"message": "Пост успешно обновлен",
	})
}

func deletePostHandler(c *gin.Context) {
	// Получаем ID поста из параметра запроса
	postID := c.Param("id")

	// Получаем текущего пользователя из контекста
	currentUserID, exists := c.Get("user")
	if !exists {
		c.JSON(http.StatusUnauthorized, gin.H{"error": "User not authenticated"})
		return
	}
	userIDString, ok := currentUserID.(string)
	if !ok {
		// Обработка случая, когда userID не является строкой
		panic("userID is not a string")
	}

	// Проверяем, является ли пользователь автором поста
	if !IsPostAuthor(postID, userIDString) {
		c.JSON(http.StatusForbidden, gin.H{"error": "You don't have permission to delete this post"})
		return
	}

	db := database.GetDB()

	// Удаляем пост из базы данных
	query := "DELETE FROM posts WHERE id = $1"
	result := db.MustExec(query, postID)

	rowsAffected, err := result.RowsAffected()
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to delete post", "err": err})
		return
	}

	if rowsAffected == 0 {
		c.JSON(http.StatusNotFound, gin.H{"error": "Post not found"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"message": "Пост успешно удален"})
}

func getPostHandler(c *gin.Context) {
	// Получаем ID поста из параметра запроса
	postID := c.Param("id")
	id, err := uuid.Parse(postID)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid post ID"})
		return
	}

	// Получаем пост из базы данных по его ID
	db := database.GetDB()
	var post models.Post
	query := "SELECT * FROM posts WHERE id = $1"
	row := db.Get(&post, query, id)
	if errors.Is(row, sql.ErrNoRows) {
		c.JSON(http.StatusNotFound, gin.H{"error": "Post not found"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"data": post})
}

func getFeedHandler(c *gin.Context) {
	// Получаем текущего пользователя из контекста
	currentUserID, exists := c.Get("user")
	if !exists {
		c.JSON(http.StatusUnauthorized, gin.H{"error": "User not authenticated"})
		return
	}

	db := database.GetDB()

	// Извлекаем параметры запроса
	offset := c.Query("offset")
	limit := c.Query("limit")

	// Валидация и преобразование параметров
	offsetInt, err := strconv.Atoi(offset)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid offset parameter"})
		return
	}

	limitInt, err := strconv.Atoi(limit)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid limit parameter"})
		return
	}

	// Используем запрос к базе данных для получения ленты новостей пользователя с учетом параметров
	queryString := `
		SELECT DISTINCT p.id, p.text, p.author_user_id, p.created_at
		FROM posts as p
		WHERE p.author_user_id IN (
			SELECT friend_id FROM friends WHERE user_id = $1
			UNION
			SELECT user_id FROM friends WHERE friend_id = $1
		)
		ORDER BY p.created_at DESC
		LIMIT $2 OFFSET $3
	`

	var feed []models.Post
	dbError := db.Select(&feed, queryString, currentUserID, limitInt, offsetInt)
	if dbError != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Error scanning all rows", "err": dbError})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"message": "Лента постов успешно получена",
		"data":    feed,
	})
}

func IsPostAuthor(postID, userID string) bool {
	db := database.GetDB()

	var authorID string
	query := "SELECT author_user_id FROM posts WHERE id = $1"
	err := db.Get(&authorID, query, postID)
	if err != nil {
		return false
	}

	return authorID == userID
}
