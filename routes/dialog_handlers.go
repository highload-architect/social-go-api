package routes

import (
	"database/sql"
	"github.com/gin-gonic/gin"
	"net/http"
	"social-go-api/database"
	"social-go-api/models"
)

func sendMessageHandler(c *gin.Context) {
	// Получаем текущего пользователя из контекста
	currentUserID, exists := c.Get("user")
	if !exists {
		c.JSON(http.StatusUnauthorized, gin.H{"error": "User not authenticated"})
		return
	}
	userIDString, ok := currentUserID.(string)
	if !ok {
		// Обработка случая, когда userID не является строкой
		panic("userID is not a string")
	}

	recipientID := c.Param("user_id")
	// Простая валидация параметра user_id
	if recipientID == "" {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": "Отсутствует идентификатор пользователя",
		})
		return
	}

	// Получаем данные из запроса
	var request struct {
		Text string `json:"text" binding:"required"`
	}

	if err := c.ShouldBindJSON(&request); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	db := database.GetDB()

	// Идентификатор получателя из пути
	var recipientExists bool
	err := db.QueryRow("SELECT EXISTS(SELECT 1 FROM users WHERE id = $1)", recipientID).Scan(&recipientExists)
	if err != nil && err != sql.ErrNoRows {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Error checking user existence"})
		return
	}
	if !recipientExists {
		c.JSON(http.StatusNotFound, gin.H{"error": "Recipient user not found"})
		return
	}

	// Создаем сообщение
	message := models.Dialog{
		FromUserID: userIDString,
		ToUserID:   recipientID,
		Text:       request.Text,
	}

	// Сохраняем сообщение в базе данных
	query := "INSERT INTO dialogs (from_user_id, to_user_id, text) VALUES ($1, $2, $3)"
	result := db.MustExec(query, message.FromUserID, message.ToUserID, message.Text)

	if result == nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Unable to save message"})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"message": "Сообщение успешно отправлено",
	})
}

func getDialogHandler(c *gin.Context) {
	// Получаем текущего пользователя из контекста
	currentUserID, exists := c.Get("user")
	if !exists {
		c.JSON(http.StatusUnauthorized, gin.H{"error": "User not authenticated"})
		return
	}
	userIDString, ok := currentUserID.(string)
	if !ok {
		// Обработка случая, когда userID не является строкой
		panic("userID is not a string")
	}

	recipientID := c.Param("user_id")
	// Простая валидация параметра user_id
	if recipientID == "" {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Отсутствует идентификатор пользователя"})
		return
	}

	db := database.GetDB()

	// Идентификатор получателя из пути
	var recipientExists bool
	err := db.QueryRow("SELECT EXISTS(SELECT 1 FROM users WHERE id = $1)", recipientID).Scan(&recipientExists)
	if err != nil && err != sql.ErrNoRows {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Error checking user existence"})
		return
	}
	if !recipientExists {
		c.JSON(http.StatusNotFound, gin.H{"error": "Recipient user not found"})
		return
	}

	// Получаем сообщения
	query := `
		SELECT id, from_user_id, to_user_id, text
		FROM dialogs
		WHERE (from_user_id = $1 AND to_user_id = $2) OR (from_user_id = $2 AND to_user_id = $1)
		ORDER BY id ASC
	`
	rows, err := db.Query(query, userIDString, recipientID)

	defer rows.Close()

	var dialog []models.Dialog
	for rows.Next() {
		var message models.Dialog
		err := rows.Scan(&message.ID, &message.FromUserID, &message.ToUserID, &message.Text)
		if err != nil {
			c.JSON(http.StatusNotFound, gin.H{"error": "Error scanning rows"})
			return
		}
		dialog = append(dialog, message)
	}

	if err := rows.Err(); err != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": "Error scanning rows"})
		return
	}

	c.JSON(http.StatusOK, gin.H{
		"message": "Диалог успешно получен",
		"data":    dialog,
	})
}
