package routes

import (
	"database/sql"
	"errors"
	"github.com/gin-gonic/gin"
	"github.com/google/uuid"
	"github.com/jmoiron/sqlx"
	"net/http"
	"social-go-api/database"
	"social-go-api/models"
)

func setFriendHandler(c *gin.Context) {
	// Получаем идентификатор пользователя из параметров пути
	userIDStr := c.Param("user_id")
	userID, err := uuid.Parse(userIDStr)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid user ID"})
		return
	}

	// Получаем текущего пользователя из контекста (например, после прохождения аутентификации)
	currentUserID, exists := c.Get("user")
	if !exists {
		c.JSON(http.StatusUnauthorized, gin.H{"error": "User not authenticated"})
		return
	}

	userIDString, ok := currentUserID.(string)
	if !ok {
		// Обработка случая, когда userID не является строкой
		panic("userID is not a string")
	}

	parsedUUID, err := uuid.Parse(userIDString)
	if err != nil {
		panic(err)
	}

	// Проверяем, что пользователь добавляет друга себе (опционально)
	if parsedUUID == userID {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Cannot add yourself as a friend"})
		return
	}

	// Проверяем наличие пользователя в базе данных
	db := database.GetDB()
	query := "SELECT id, first_name FROM users WHERE id = $1"
	var user models.User
	row := db.Get(&user, query, userID)
	if errors.Is(row, sql.ErrNoRows) {
		c.JSON(http.StatusNotFound, gin.H{"error": "User not found"})
		return
	}

	// Проверяем, не является ли пользователь уже другом
	isFriend, err := checkIfFriends(db, parsedUUID, userID)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Error checking friendship status"})
		return
	}
	if isFriend {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Users are already friends"})
		return
	}
	// Добавляем друга в базу данных
	_, err = db.Exec("INSERT INTO friends (user_id, friend_id) VALUES ($1, $2)", parsedUUID, userID)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Error adding friend to the database"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"message": "Friend added successfully"})
}

func deleteFriendHandler(c *gin.Context) {
	// Получаем идентификатор пользователя из параметров пути
	userIDStr := c.Param("user_id")
	userID, err := uuid.Parse(userIDStr)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid user ID"})
		return
	}

	// Получаем текущего пользователя из контекста (например, после прохождения аутентификации)
	currentUserID, exists := c.Get("user")
	if !exists {
		c.JSON(http.StatusUnauthorized, gin.H{"error": "User not authenticated"})
		return
	}

	userIDString, ok := currentUserID.(string)
	if !ok {
		// Обработка случая, когда userID не является строкой
		panic("userID is not a string")
	}

	parsedUUID, err := uuid.Parse(userIDString)
	if err != nil {
		panic(err)
	}

	// Проверяем, что пользователь добавляет друга себе (опционально)
	if parsedUUID == userID {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Cannot add yourself as a friend"})
		return
	}

	db := database.GetDB()

	// Проверяем наличие пользователя в базе данных
	var user models.User
	err = db.Get(&user, "SELECT * FROM users WHERE id = $1", userID)
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{"error": "User not found"})
		return
	}

	// Проверяем, являются ли пользователи друзьями
	isFriend, err := checkIfFriends(db, parsedUUID, userID)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Error checking friendship status"})
		return
	}
	if !isFriend {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Users are not friends"})
		return
	}

	// Удаляем друга из базы данных
	_, err = db.Exec("DELETE FROM friends WHERE user_id = $1 AND friend_id = $2", parsedUUID, userID)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Error removing friend from the database"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"message": "Friend deleted successfully"})
}

// checkIfFriends проверяет, являются ли два пользователя друзьями
func checkIfFriends(db *sqlx.DB, userID uuid.UUID, friendID uuid.UUID) (bool, error) {
	// Провести запрос к базе данных, чтобы проверить, являются ли пользователи друзьями
	query := "SELECT COUNT(*) FROM friends WHERE (user_id = $1 AND friend_id = $2) OR (user_id = $2 AND friend_id = $1)"
	var count int
	err := db.Get(&count, query, userID, friendID)
	if err != nil {
		return false, err
	}

	// Если count больше 0, то пользователи являются друзьями
	return count > 0, nil
}
