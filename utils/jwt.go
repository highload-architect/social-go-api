package utils

import (
	"errors"
	"github.com/dgrijalva/jwt-go"
	"os"
	"social-go-api/models"
	"time"
)

// GenerateToken создает новый токен для пользователя
func GenerateToken(user models.User) (string, error) {
	// Устанавливаем срок действия токена
	expirationTime := time.Now().Add(24 * time.Hour) // Например, токен действителен 24 часа

	// Создаем токен
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, jwt.MapClaims{
		"userId": user.ID.String(),
		"exp":    expirationTime.Unix(),
	})

	// Подписываем токен с использованием секретного ключа
	secretKey := []byte(os.Getenv("JWT_SECRET"))
	tokenString, err := token.SignedString(secretKey)
	if err != nil {
		return "", err
	}

	return tokenString, nil
}

// ValidateToken проверяет валидность токена и возвращает идентификатор пользователя
func ValidateToken(tokenString string) (string, error) {
	secretKey := []byte(os.Getenv("JWT_SECRET"))

	// Парсим токен
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		// Проверяем метод подписи
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, errors.New("invalid signing method")
		}
		return secretKey, nil
	})
	if err != nil {
		return "", err
	}

	// Проверяем валидность токена
	if !token.Valid {
		return "", errors.New("invalid token")
	}

	// Извлекаем идентификатор пользователя из токена
	claims, ok := token.Claims.(jwt.MapClaims)
	if !ok {
		return "", errors.New("invalid token claims")
	}

	userID, ok := claims["userId"].(string)
	if !ok {
		return "", errors.New("invalid userId in token")
	}

	return userID, nil
}
