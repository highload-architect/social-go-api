import {Links, LiveReload, Meta, Outlet, Scripts,} from "@remix-run/react";
import {LinksFunction} from "@remix-run/node";

// @ts-ignore
import stylesheet from "./tailwind.css";
import Menu from "~/components/Menu";
import {AuthProvider} from "~/utils/authContext";

export const links: LinksFunction = () => [
    {rel: "stylesheet", href: stylesheet},
];

export default function App() {
    return (
        <html>
        <head>
            <link
                rel="icon"
                href="data:image/x-icon;base64,AA"
            />
            <Meta/>
            <Links/>
        </head>
        <body>
        <AuthProvider>
            <div className="h-screen bg-gray-900 text-white">
                <Menu/>
                <div className="flex flex-col items-center justify-start">
                    <Outlet/>
                </div>
                <Scripts/>
                <LiveReload/>
            </div>
        </AuthProvider>
        </body>
        </html>
    )
        ;
}
