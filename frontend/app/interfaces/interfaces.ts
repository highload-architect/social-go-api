export interface User {
    id: string;
    email: string;
    first_name: string;
    second_name: string;
    birthdate: string;
    biography: string;
    city: string;
    created_at: string;
}

export interface Dialog {
    id: number;
    from_user_id: string;
    to_user_id: string;
    text: string;
    created_at: string;
}

export interface Post {
    id: string;
    text: string;
    author_user_id: string;
    created_at: string;
}
