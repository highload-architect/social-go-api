import PostCreate from "~/components/PostCreate";
import PostFeed from "~/components/PostFeed";
import {useEffect, useState} from "react";
import {Post} from "~/interfaces/interfaces";
import {useAuth} from "~/utils/authContext";
import axiosInstance from "~/utils/axiosInstance";

export default function PostsLayout() {
    const [posts, setPosts] = useState<Post[]>([]);
    const [error, setError] = useState<string | null>(null);
    const [isLoading, setIsLoading] = useState(false);

    const {token} = useAuth();

    const fetchPosts = async () => {
        try {
            setIsLoading(true);
            setError(null);

            const response = await axiosInstance.get('/post/feed?offset=0&limit=20', {
                headers: {
                    Authorization: `Bearer ${token}`,
                },
            });
            setPosts(response.data.data);
        } catch (error: any) {
            console.error('Failed to fetch posts:', error.message);
            setError('Failed to fetch posts. Please try again later.');
        } finally {
            setIsLoading(false);
        }
    };

    useEffect(() => {
        void fetchPosts();
    }, []);

    return (
        <div>
            <h1 className="text-3xl w-full text-center p-10">Posts</h1>
            <div className="grid grid-cols-5 gap-4 min-h-5 min-w-[1200px]">
                <div className="col-start-1 col-end-3">
                    <PostCreate fetchPosts={fetchPosts} />
                </div>
                <div className="col-start-3 col-end-6">
                    <PostFeed posts={posts} isLoading={isLoading} error={error}/>
                </div>
            </div>
        </div>
    );
}
