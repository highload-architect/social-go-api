import {useParams} from "@remix-run/react";
import {useEffect, useState} from "react";
import {User} from "~/interfaces/interfaces";
import axiosInstance from "~/utils/axiosInstance";
import {useAuth} from "~/utils/authContext";
import DialogChat from "~/components/DialogChat";

export default function DialogUserPage() {
    const {userId} = useParams<{ userId: string }>();
    const [user, setUser] = useState<User | null>(null);
    const [dialog, setDialog] = useState<null>(null);
    const [error, setError] = useState<string | null>(null);
    const [isLoadingUser, setIsLoadingUser] = useState(false);
    const [isLoadingDialog, setIsLoadingDialog] = useState(false);
    const [text, setText] = useState<string>('');
    const [sending, setSending] = useState(false);

    const {token} = useAuth();

    const fetchDialog = async () => {
        try {
            setIsLoadingDialog(true);
            setError(null);

            const response = await axiosInstance.get(`/dialog/${userId}`, {
                headers: {
                    Authorization: `Bearer ${token}`,
                },
            });

            setDialog(response.data.data);
        } catch (error: any) {
            console.error('Search failed:', error.message);
            setError('Search failed. Please try again later.');
        } finally {
            setIsLoadingDialog(false);
        }
    };

    useEffect(() => {
        const init = async () => {
            await fetchUserProfile()
            void fetchDialog();
        }

        const fetchUserProfile = async () => {
            try {
                setIsLoadingUser(true);
                setError(null);

                const response = await axiosInstance.get(`/user/${userId}`, {
                    headers: {
                        Authorization: `Bearer ${token}`,
                    },
                });

                setUser(response.data);
            } catch (error: any) {
                console.error('Failed to fetch user profile:', error.message);
                const uiError = error?.response?.data?.error;
                setError(`Failed to fetch user profile: ${uiError || 'Please try again later.'}`);
            } finally {
                setIsLoadingUser(false);
            }
        };

        void init()
    }, [userId]);

    const handleSendMessage = async (e: React.FormEvent) => {
        e.preventDefault();
        setError(null);
        try {
            setSending(true);

            const response = await axiosInstance.post(`/dialog/${user?.id}`, {
                    text,
                },
                {
                    headers: {
                        Authorization: `Bearer ${token}`,
                    },
                });

            console.log('Send successful!', response.data);
            setText('');
            void fetchDialog();
        } catch (error: any) {
            console.error('Send message failed:', error.message);
            setError('Send message failed.');
        } finally {
            setSending(false);
        }
    };

    return (
        <div className="flex flex-col">
            {error && <div>Error: {error}</div>}

            <header className="col-start-1 col-end-3 row-start-1 row-end-2">
                <h2 className="text-2xl w-full text-center p-2 h-12 pb-4">
                    {isLoadingUser && 'Loading...'}
                    {!isLoadingUser && `${user?.first_name} ${user?.second_name}`}
                </h2>
            </header>

            <DialogChat dialog={dialog}/>

            <footer className="row-auto p-4 w-full">
                <form method="post" className="flex flex-col"
                      onSubmit={handleSendMessage}
                >
                    <input type="text" name="message" placeholder="Message"
                           className="py-2 px-4 bg-gray-800 text-gray-300 rounded-md focus:outline-none mb-4" required
                           value={text}
                           onChange={e => setText(e.target.value)}
                    />
                    <button type="submit"
                            className="bg-blue-500 py-2 px-4 text-gray-300 rounded-md hover:bg-blue-600 focus:outline-none w-full"
                            disabled={sending}
                    >
                        {sending ? 'Sending...' : 'Send'}
                    </button>
                </form>
            </footer>
        </div>
    );
}
