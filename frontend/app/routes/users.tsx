import {Outlet} from "@remix-run/react";
import UsersList from "~/components/UserList";
import UsersSearch from "~/components/UserSearch";
import {useEffect, useState} from "react";
import {User} from "~/interfaces/interfaces";

export default function UsersLayout() {
    const [searchResults, setSearchResults] = useState<User[]>([]);

    useEffect(() => {
        const users = localStorage.getItem('searchedUsers');
        if (users) {
            let parsedUsers;
            try {
                parsedUsers = JSON.parse(users);
                setSearchResults(parsedUsers);
            } catch (e) {
                localStorage.setItem('searchedUsers', '');
            }
        }
    }, []);

    return (
        <div>
            <h1 className="text-3xl w-full text-center p-10">Users</h1>
            <div className="grid grid-cols-5 gap-4 min-h-5 min-w-[1200px]">
                <div className="col-span-5 row-span-1 pb-2">
                    <UsersSearch setSearchResults={setSearchResults}/>
                </div>
                <div className="flex flex-col col-span-2 gap-10">
                    <UsersList searchResults={searchResults}/>
                </div>
                <div className="col-span-3">
                    <Outlet/>
                </div>
            </div>
        </div>
    );
}
