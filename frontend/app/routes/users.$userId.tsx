import {useEffect, useState} from "react";
import {useParams} from "@remix-run/react";

import {User} from "~/interfaces/interfaces";
import axiosInstance from "~/utils/axiosInstance";
import {useAuth} from "~/utils/authContext";

export default function UserProfilePage() {
    const {userId} = useParams<{ userId: string }>();
    const [user, setUser] = useState<User | null>(null);
    const [error, setError] = useState<string | null>(null);
    const [isLoading, setIsLoading] = useState(false);
    const [isAddingFriend, setIsAddingFriend] = useState(false);

    const {token} = useAuth();

    useEffect(() => {
        const fetchUserProfile = async () => {
            try {
                setIsLoading(true);
                setError(null);

                const response = await axiosInstance.get(`/user/${userId}`, {
                    headers: {
                        Authorization: `Bearer ${token}`,
                    },
                });

                setUser(response.data);
            } catch (error: any) {
                console.error('Failed to fetch user profile:', error.message);
                const uiError = error?.response?.data?.error;
                setError(`Failed to fetch user profile: ${uiError || 'Please try again later.'}`);
            } finally {
                setIsLoading(false);
            }
        };

        void fetchUserProfile();
    }, [userId]);

    const handleAddFriend = async (userId: string) => {
        try {
            setIsAddingFriend(true);
            setError(null);

            // Отправка запроса на сервер для добавления друга.
            await axiosInstance.post(`/friend/${userId}`,
                {
                    friendId: userId,
                }, {
                    headers: {
                        Authorization: `Bearer ${token}`,
                    },
                }
            );

            console.log('Friend request sent successfully!');
        } catch (error: any) {
            console.error('Failed to send friend request:', error.message);
            const uiError = error?.response?.data?.error;
            setError(uiError || 'Failed to send friend request. Please try again later.');
        } finally {
            setIsAddingFriend(false);
        }
    };

    return (
        <div className="flex flex-col gap-4">
            {isLoading && <p>Loading...</p>}
            {error && <p>Error: {error}</p>}
            {user && (
                <>
                    <div className="flex flex-col sticky top-0 z-10">
                        <div className="bg-gray-800 border border-gray-800 shadow-lg  rounded-2xl p-4">
                            <div className="flex-none sm:flex">
                                <div className=" relative h-32 w-32   sm:mb-0 mb-3">
                                    <img
                                        src="https://tailwindcomponents.com/storage/avatars/njkIbPhyZCftc4g9XbMWwVsa7aGVPajYLRXhEeoo.jpg"
                                        alt="aji" className=" w-32 h-32 object-cover rounded-2xl"/>
                                    {/*<a href="#"*/}
                                    {/*   className="absolute -right-2 bottom-2   -ml-3  text-white p-1 text-xs bg-green-400 hover:bg-green-500 font-medium tracking-wider rounded-full transition ease-in duration-300">*/}
                                    {/*    <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20" fill="currentColor"*/}
                                    {/*         className="h-4 w-4">*/}
                                    {/*        <path*/}
                                    {/*            d="M13.586 3.586a2 2 0 112.828 2.828l-.793.793-2.828-2.828.793-.793zM11.379 5.793L3 14.172V17h2.828l8.38-8.379-2.83-2.828z">*/}
                                    {/*        </path>*/}
                                    {/*    </svg>*/}
                                    {/*</a>*/}
                                </div>
                                <div className="flex-auto sm:ml-5 justify-evenly">
                                    <div className="flex items-center justify-between sm:mt-2">
                                        <div className="flex items-center">
                                            <div className="flex flex-col">
                                                <div
                                                    className="w-full flex-none text-lg text-gray-200 font-bold leading-none">
                                                    {user.first_name} {user.second_name}
                                                </div>
                                                <div className="flex-auto text-gray-400 my-1">
                                                    <span className="mr-3 ">{user.city}</span><span
                                                    className="mr-3 border-r border-gray-600  max-h-0"></span><span>{user.birthdate}</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="flex flex-row items-center">
                                        <div className="flex">
                                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"
                                                 fill="currentColor"
                                                 className="h-5 w-5 text-yellow-400">
                                                <path
                                                    d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z">
                                                </path>
                                            </svg>
                                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"
                                                 fill="currentColor"
                                                 className="h-5 w-5 text-yellow-400">
                                                <path
                                                    d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z">
                                                </path>
                                            </svg>
                                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"
                                                 fill="currentColor"
                                                 className="h-5 w-5 text-yellow-400">
                                                <path
                                                    d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z">
                                                </path>
                                            </svg>
                                            <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 20 20"
                                                 fill="currentColor"
                                                 className="h-5 w-5 text-yellow-400">
                                                <path
                                                    d="M9.049 2.927c.3-.921 1.603-.921 1.902 0l1.07 3.292a1 1 0 00.95.69h3.462c.969 0 1.371 1.24.588 1.81l-2.8 2.034a1 1 0 00-.364 1.118l1.07 3.292c.3.921-.755 1.688-1.54 1.118l-2.8-2.034a1 1 0 00-1.175 0l-2.8 2.034c-.784.57-1.838-.197-1.539-1.118l1.07-3.292a1 1 0 00-.364-1.118L2.98 8.72c-.783-.57-.38-1.81.588-1.81h3.461a1 1 0 00.951-.69l1.07-3.292z">
                                                </path>
                                            </svg>
                                            <svg xmlns="http://www.w3.org/2000/svg" fill="none" viewBox="0 0 24 24"
                                                 stroke="currentColor" className="h-5 w-5 text-yellow-400">
                                                <path strokeLinecap="round" strokeLinejoin="round" strokeWidth="2"
                                                      d="M11.049 2.927c.3-.921 1.603-.921 1.902 0l1.519 4.674a1 1 0 00.95.69h4.915c.969 0 1.371 1.24.588 1.81l-3.976 2.888a1 1 0 00-.363 1.118l1.518 4.674c.3.922-.755 1.688-1.538 1.118l-3.976-2.888a1 1 0 00-1.176 0l-3.976 2.888c-.783.57-1.838-.197-1.538-1.118l1.518-4.674a1 1 0 00-.363-1.118l-3.976-2.888c-.784-.57-.38-1.81.588-1.81h4.914a1 1 0 00.951-.69l1.519-4.674z">
                                                </path>
                                            </svg>
                                        </div>
                                    </div>
                                    <div className="flex pt-2  text-sm text-gray-400">
                                        <div className="flex-1 inline-flex items-center">
                                            <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5 mr-2"
                                                 viewBox="0 0 20 20"
                                                 fill="currentColor">
                                                <path
                                                    d="M8 9a3 3 0 100-6 3 3 0 000 6zM8 11a6 6 0 016 6H2a6 6 0 016-6zM16 7a1 1 0 10-2 0v1h-1a1 1 0 100 2h1v1a1 1 0 102 0v-1h1a1 1 0 100-2h-1V7z">
                                                </path>
                                            </svg>
                                            <p className="">1.2k Followers</p>
                                        </div>
                                        <div className="flex-1 inline-flex items-center">
                                            <svg xmlns="http://www.w3.org/2000/svg" className="h-5 w-5 mr-2"
                                                 viewBox="0 0 20 20"
                                                 fill="currentColor">
                                                <path fillRule="evenodd"
                                                      d="M12.395 2.553a1 1 0 00-1.45-.385c-.345.23-.614.558-.822.88-.214.33-.403.713-.57 1.116-.334.804-.614 1.768-.84 2.734a31.365 31.365 0 00-.613 3.58 2.64 2.64 0 01-.945-1.067c-.328-.68-.398-1.534-.398-2.654A1 1 0 005.05 6.05 6.981 6.981 0 003 11a7 7 0 1011.95-4.95c-.592-.591-.98-.985-1.348-1.467-.363-.476-.724-1.063-1.207-2.03zM12.12 15.12A3 3 0 017 13s.879.5 2.5.5c0-1 .5-4 1.25-4.5.5 1 .786 1.293 1.371 1.879A2.99 2.99 0 0113 13a2.99 2.99 0 01-.879 2.121z"
                                                      clipRule="evenodd"></path>
                                            </svg>
                                            <p className="">{user.email}</p>
                                        </div>
                                        <button
                                            className="flex-no-shrink bg-green-800 hover:bg-green-600 px-5 ml-4 py-2 text-xs shadow-sm hover:shadow-lg font-medium tracking-wider border-2 border-green-800 hover:border-green-600 text-white rounded-full transition ease-in duration-300"
                                            onClick={() => handleAddFriend(user.id)} disabled={isAddingFriend}>
                                            {isAddingFriend ? 'Adding...' : 'Add Friend'}
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="grid grid-cols-12 gap-4 ">
                        <div className="col-span-12 sm:col-span-4">
                            <div className="p-4 relative  bg-gray-800 border border-gray-800 shadow-lg  rounded-2xl">
                                <svg xmlns="http://www.w3.org/2000/svg"
                                     className="h-14 w-14  absolute bottom-4 right-3 text-green-400" viewBox="0 0 20 20"
                                     fill="currentColor">
                                    <path d="M2 10a8 8 0 018-8v8h8a8 8 0 11-16 0z"/>
                                    <path d="M12 2.252A8.014 8.014 0 0117.748 8H12V2.252z"/>
                                </svg>
                                <div className="text-2xl text-gray-100 font-medium leading-8 mt-5">20</div>
                                <div className="text-sm text-gray-500">Components</div>
                            </div>
                        </div>
                        <div className="col-span-12 sm:col-span-4">
                            <div className="p-4 relative  bg-gray-800 border border-gray-800 shadow-lg  rounded-2xl">
                                <svg xmlns="http://www.w3.org/2000/svg"
                                     className="h-14 w-14  absolute bottom-4 right-3 text-blue-500" viewBox="0 0 20 20"
                                     fill="currentColor">
                                    <path
                                        d="M2 11a1 1 0 011-1h2a1 1 0 011 1v5a1 1 0 01-1 1H3a1 1 0 01-1-1v-5zM8 7a1 1 0 011-1h2a1 1 0 011 1v9a1 1 0 01-1 1H9a1 1 0 01-1-1V7zM14 4a1 1 0 011-1h2a1 1 0 011 1v12a1 1 0 01-1 1h-2a1 1 0 01-1-1V4z"/>
                                </svg>
                                <div className="flex justify-between items-center ">
                                    <i className="fab fa-behance text-xl text-gray-400"></i>
                                </div>
                                <div className="text-2xl text-gray-100 font-medium leading-8 mt-5">99</div>
                                <div className="text-sm text-gray-500">Projects</div>
                            </div>
                        </div>
                        <div className="col-span-12 sm:col-span-4">
                            <div className="p-4 relative  bg-gray-800 border border-gray-800 shadow-lg  rounded-2xl">
                                <svg xmlns="http://www.w3.org/2000/svg"
                                     className="h-14 w-14  absolute bottom-4 right-3 text-yellow-300"
                                     viewBox="0 0 20 20"
                                     fill="currentColor">
                                    <path fillRule="evenodd"
                                          d="M3 3a1 1 0 000 2v8a2 2 0 002 2h2.586l-1.293 1.293a1 1 0 101.414 1.414L10 15.414l2.293 2.293a1 1 0 001.414-1.414L12.414 15H15a2 2 0 002-2V5a1 1 0 100-2H3zm11.707 4.707a1 1 0 00-1.414-1.414L10 9.586 8.707 8.293a1 1 0 00-1.414 0l-2 2a1 1 0 101.414 1.414L8 10.414l1.293 1.293a1 1 0 001.414 0l4-4z"
                                          clipRule="evenodd"/>
                                </svg>
                                <div className="flex justify-between items-center ">
                                    <i className="fab fa-codepen text-xl text-gray-400"></i>
                                </div>
                                <div className="text-2xl text-gray-100 font-medium leading-8 mt-5">50</div>
                                <div className="text-sm text-gray-500">Pen Items</div>
                            </div>
                        </div>
                    </div>
                </>)}
        </div>
    );
}
