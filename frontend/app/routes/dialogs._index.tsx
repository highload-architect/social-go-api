export default function DialogIndexPage() {
    return (
        <div className="w-full">
            <div
                className="flex flex-col justify-center p-6 bg-gray-800 border-gray-800 shadow-md hover:shodow-lg rounded-2xl mt-16">
                <div className="flex items-center justify-between">
                    <div className="flex flex-col items-center w-full">
                        <div className="font-medium leading-none text-gray-100">Choose user to chat</div>
                        <p className="text-sm text-gray-500 leading-none mt-1">From list on left</p>
                    </div>
                </div>
            </div>
        </div>
    );
}
