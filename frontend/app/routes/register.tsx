import axiosInstance from "~/utils/axiosInstance";
import {useState} from "react";
import {useNavigate} from "@remix-run/react";

export default function RegisterPage() {
    const [formData, setFormData] = useState({
        email: '',
        password: '',
        first_name: '',
        second_name: '',
        birthdate: '',
        biography: '',
        city: '',
    });
    const [error, setError] = useState<string | null>(null);
    const [isLoading, setIsLoading] = useState(false);
    const navigate = useNavigate();

    const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const {name, value} = e.target;
        setFormData((prevData: any) => ({
            ...prevData,
            [name]: value,
        }));
    };

    const handleSubmit = async (e: React.FormEvent) => {
        e.preventDefault();
        try {
            setIsLoading(true);
            setError(null);

            // Отправка запроса на сервер для регистрации
            const response = await axiosInstance.post('/auth/register', formData);

            // Обработка успешной регистрации, например, перенаправление пользователя
            console.log('Registration successful:', response.data);
            navigate('/users');
        } catch (error: any) {
            console.error('Registration failed:', error.message);
            setError('Registration failed. Please try again later.');
        } finally {
            setIsLoading(false);
        }
    };

    return (
        <div>
            <h1 className="text-3xl w-full text-center p-10">Register</h1>
            <form onSubmit={handleSubmit} method="post" className="flex flex-col">
                <div className="grid grid-cols-2 gap-2">
                    <input type="email" name="email" placeholder="Email"
                           className="py-2 px-4 bg-gray-800 text-white rounded-md focus:outline-none mb-4" required
                           value={formData.email}
                           onChange={handleChange}
                    />
                    <input type="password" name="password" placeholder="Password"
                           className="py-2 px-4 bg-gray-800 text-white rounded-md focus:outline-none mb-4"
                           value={formData.password}
                           onChange={handleChange}
                    />
                    <input type="text" name="first_name" placeholder="First name"
                           className="py-2 px-4 bg-gray-800 text-white rounded-md focus:outline-none mb-4"
                           value={formData.first_name}
                           onChange={handleChange}
                    />
                    <input type="text" name="second_name" placeholder="Second name"
                           className="py-2 px-4 bg-gray-800 text-white rounded-md focus:outline-none mb-4"
                           value={formData.second_name}
                           onChange={handleChange}
                    />
                    <input type="date" name="birthdate" placeholder="Birthdate"
                           className="py-2 px-4 bg-gray-800 text-white rounded-md focus:outline-none mb-4 w-full"
                           value={formData.birthdate}
                           onChange={handleChange}
                    />
                    <input type="text" name="city" placeholder="City"
                           className="py-2 px-4 bg-gray-800 text-white rounded-md focus:outline-none mb-4"
                           value={formData.city}
                           onChange={handleChange}
                    />
                    <textarea name="biography" placeholder="Biography" rows={2}
                              className="py-2 px-4 bg-gray-800 text-white rounded-md focus:outline-none mb-4 resize-none w-full col-span-2"
                              value={formData.biography}
                              onChange={e => setFormData((prevData: any) => ({
                                  ...prevData,
                                  biography: e.target.value,
                              }))}
                    />
                </div>
                <button type="submit"
                        className="bg-blue-500 py-2 px-4 text-white rounded-md hover:bg-blue-600 focus:outline-none w-full"
                        disabled={isLoading}
                >
                    {isLoading ? 'Registering...' : 'Register'}
                </button>
                {error && <p>Error: {error}</p>}
            </form>
        </div>
    );
}
