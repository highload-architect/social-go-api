import {Outlet} from "@remix-run/react";
import DialogList from "~/components/DialogList";
import {useEffect, useState} from "react";
import {User} from "~/interfaces/interfaces";

export default function DialogsLayout() {
    const [searchResults, setSearchResults] = useState<User[]>([]);

    useEffect(() => {
        const users = localStorage.getItem('searchedUsers');
        if (users) {
            let parsedUsers;
            try {
                parsedUsers = JSON.parse(users);
                setSearchResults(parsedUsers);
            } catch (e) {
                localStorage.setItem('searchedUsers', '');
            }
        }
    }, []);

    return (
        <div>
            <h1 className="text-3xl w-full text-center p-10">Dialogs</h1>
            <div className="grid grid-cols-5 gap-4 min-h-5 min-w-[1200px]">
                <div className="col-start-1 col-end-3 row-start-1 row-end-2">
                    <h2 className="text-2xl w-full text-center p-2 h-12 pb-4">Chat list</h2>
                </div>
                <div className="col-start-1 col-end-3 row-start-2 row-span-3">
                    <DialogList searchResults={searchResults}/>
                </div>
                <div className="col-start-3 col-end-6 row-start-1 row-end-3">
                    <Outlet/>
                </div>
            </div>
        </div>
    );
}
