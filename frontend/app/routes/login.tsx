import {useState} from "react";
import axiosInstance from '~/utils/axiosInstance';
import {useAuth} from "~/utils/authContext";
import {useNavigate} from "@remix-run/react";

export default function LoginPage() {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [error, setError] = useState<string | null>(null);
    const [isLoading, setIsLoading] = useState(false);
    const navigate = useNavigate();

    const {setToken, setUserId} = useAuth();

    const handleSubmit = async (e: React.FormEvent) => {
        e.preventDefault();
        setError(null);
        try {
            setIsLoading(true);

            const response = await axiosInstance.post('/auth/login', {
                email,
                password,
            });

            const {token, userId} = response.data;

            console.log('Login successful!', response.data);
            setToken(token);
            setUserId(userId);
            navigate('/users');
        } catch (error: any) {
            console.error('Login failed:', error.message);
            setError('Login failed. Please check your credentials.');
        } finally {
            setIsLoading(false);
        }
    };

    return (
        <div>
            <h1 className="text-3xl w-full text-center p-10">Login</h1>
            <form className="flex flex-col items-center" onSubmit={handleSubmit}>
                <input type="email" name="email" placeholder="Email"
                       className="py-2 px-4 bg-gray-800 text-white rounded-md focus:outline-none mb-4" required
                       value={email}
                       onChange={(e) => setEmail(e.target.value)}
                />
                <input type="text" name="password" placeholder="Password"
                       className="py-2 px-4 bg-gray-800 text-white rounded-md focus:outline-none mb-4" required
                       value={password}
                       onChange={(e) => setPassword(e.target.value)}
                />
                <button type="submit"
                        className="bg-blue-500 py-2 px-4 text-white rounded-md hover:bg-blue-600 focus:outline-none w-full"
                        disabled={isLoading}
                >
                    {isLoading ? 'Logging in...' : 'Login'}
                </button>
                {error && <div>Error: {error}</div>}
            </form>
        </div>
    );
}
