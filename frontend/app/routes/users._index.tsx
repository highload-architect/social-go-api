export default function UsersIndexPage() {
    return (
        <div className="w-full">
            <div
                className="flex flex-col justify-center p-6 bg-gray-800 border-gray-800 shadow-md rounded-2xl ">
                <div className="flex items-center justify-between">
                    <div className="flex flex-col items-center w-full">
                        <div className="font-medium leading-none text-gray-100">Search and choose user</div>
                        <p className="text-sm text-gray-500 leading-none mt-1">With form on top</p>
                    </div>
                </div>
            </div>
        </div>
    );
}
