import {Link} from "@remix-run/react";

export default function IndexPage() {
    return (
        <div className="flex flex-col items-center">
            <h1 className="p-4 text-5xl">Social APP</h1>

            <div className="py-12 relative overflow-hidden ">
                <div className="grid grid-cols-2 max-w-screen-lg mx-auto">
                    <div
                        className="py-20 relative">
                        <div className="relative z-20 pl-12">
                            <h2 className="text-[#f7d0b6] font-black text-5xl leading-snug mb-10">Sign In</h2>
                            <p className="text-white text-sm">
                                Purus in massa tempor nec. Magna etiam tempor orci eu lobortis elementum nibh tellus
                                molestie. Faucibus ornare suspendisse sed nisi lacus sed viverra. Diam in arcu cursus
                                euismod quis viverra nibh cras pulvinar.
                            </p>
                            <Link to="/login">
                                <button
                                    className="mt-8 text-white uppercase py-3 text-sm px-10 border border-white hover:bg-white hover:bg-opacity-10">
                                    Login
                                </button>
                            </Link>
                        </div>
                    </div>

                    <div
                        className="py-20 bg-blue-950 relative">
                        <div className="relative z-20 pl-12">
                            <h2 className="text-[#f7d0b6] font-black text-5xl leading-snug mb-10">Sign Up</h2>
                            <p className="text-white text-sm">
                                Purus in massa tempor nec. Magna etiam tempor orci eu lobortis elementum nibh tellus
                                molestie. Faucibus ornare suspendisse sed nisi lacus sed viverra. Diam in arcu cursus
                                euismod quis viverra nibh cras pulvinar.
                            </p>
                            <Link to="/register">
                                <button
                                    className="mt-8 text-white uppercase py-3 text-sm px-10 border border-white hover:bg-white hover:bg-opacity-10">
                                    Register
                                </button>
                            </Link>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}
