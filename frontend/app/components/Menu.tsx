import {Link} from "@remix-run/react";
import {useAuth} from "~/utils/authContext";

export default function Menu() {
    const {token} = useAuth();

    return (
        <nav className="flex justify-center w-full pt-2">
            <ul className="flex items-center justify-center gap-4 p-4 w-full">
                <li><Link className="text-lg p-3 hover:text-green-400 border border-gray-600 rounded-md" to={`/`}>Social APP</Link></li>
                {!token &&
                    <>
                        <li><Link className="text-lg p-2 hover:text-green-400" to={`/login`}>Login</Link></li>
                        <li><Link className="text-lg p-2 hover:text-green-400" to={`/register`}>Register</Link></li>
                    </>
                }
                {!!token &&
                    <>
                        <li><Link className="text-lg p-2 hover:text-green-400" to={`/users`}>Users</Link></li>
                        <li><Link className="text-lg p-2 hover:text-green-400" to={`/posts`}>Posts</Link></li>
                        <li><Link className="text-lg p-2 hover:text-green-400" to={`/dialogs`}>Dialogs</Link></li>
                    </>
                }
            </ul>
        </nav>
    );
}


