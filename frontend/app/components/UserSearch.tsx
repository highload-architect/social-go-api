import axiosInstance from '../utils/axiosInstance';
import {useAuth} from '~/utils/authContext';
import {useEffect, useState} from "react";


export default function UsersSearch({setSearchResults}: any) {
    const [email, setEmail] = useState('');
    const [firstName, setFirstName] = useState('');
    const [secondName, setSecondName] = useState('');
    const [error, setError] = useState<string | null>(null);
    const [isLoading, setIsLoading] = useState(false);

    const {token} = useAuth();

    const handleSearch = async () => {
        try {
            setIsLoading(true);
            setError(null);

            // Сохранение параметров поиска в localStorage
            localStorage.setItem('userSearchEmail', email);
            localStorage.setItem('userSearchFirstName', firstName);
            localStorage.setItem('userSearchSecondName', secondName);

            const response = await axiosInstance.get('/user/search', {
                headers: {
                    Authorization: `Bearer ${token}`,
                },
                params: {
                    email: email,
                    first_name: firstName,
                    second_name: secondName,
                },
            });

            setSearchResults(response.data);

            // Сохранение пользователей в localStorage
            localStorage.setItem('searchedUsers', JSON.stringify(response.data));
        } catch (error: any) {
            console.error('Search failed:', error.message);
            setError('Search failed. Please try again later.');
        } finally {
            setIsLoading(false);
        }
    };

    useEffect(() => {
        const email = localStorage.getItem('userSearchEmail');
        const firstName = localStorage.getItem('userSearchFirstName');
        const secondName = localStorage.getItem('userSearchSecondName');
        if (email || firstName || secondName) void handleSearch();
    }, []);

    return (
        <div>
            <form method="post" className="flex flex-row items-center gap-4"
                  onSubmit={handleSearch}
            >
                <input type="text" name="email" placeholder="Email"
                       className="py-2 px-4 bg-gray-800 text-white rounded-md focus:outline-none"
                       value={email}
                       onChange={e => setEmail(e.target.value)}
                />
                <input type="text" name="first_name" placeholder="First name"
                       className="py-2 px-4 bg-gray-800 text-white rounded-md focus:outline-none"
                       value={firstName}
                       onChange={e => setFirstName(e.target.value)}
                />
                <input type="text" name="second_name" placeholder="Second name"
                       className="py-2 px-4 bg-gray-800 text-white rounded-md focus:outline-none"
                       value={secondName}
                       onChange={e => setSecondName(e.target.value)}
                />
                <button type="submit"
                        className="bg-blue-500 py-2 px-4 text-white rounded-md hover:bg-blue-600 focus:outline-none"
                        onClick={handleSearch} disabled={isLoading}
                >
                    {isLoading ? 'Searching...' : 'Search'}
                </button>
                {error && <div>Error: {error}</div>}
            </form>
        </div>
    );
}



