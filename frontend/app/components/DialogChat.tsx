import {Dialog} from "~/interfaces/interfaces";
import DialogMessage from "~/components/DialogMessage";

interface Props {
    dialog: Dialog[] | null
}

export default function DialogChat({dialog}: Props) {
    const dialogs = Array.isArray(dialog) && dialog.map(message => <DialogMessage key={message.id} message={message}/>)

    return (
        <div className="overflow-y-auto p-4 max-h-96">
            {dialog && !!dialog.length && dialogs}
            {!dialog && <div
                className="flex flex-col justify-center p-5 bg-gray-800 border-gray-800 shadow-md rounded-2xl ">
                <div className="flex items-center justify-between">
                    <div className="flex flex-col items-center w-full">
                        <div className="font-medium leading-none text-gray-100">No messages</div>
                        <p className="text-sm text-gray-500 leading-none mt-1">Start chatting!</p>
                    </div>
                </div>
            </div>}
        </div>
    );
}



