import UsersListElement from "~/components/UserListElement";
import {User} from "~/interfaces/interfaces";

interface Props {
    searchResults: User[]
}

export default function UsersList({searchResults}: Props) {
    const usersElements = searchResults.map((user) => <UsersListElement key={user.id} user={user}/>)

    return (
        <div className="flex flex-col gap-4">
            {usersElements.length > 0 && usersElements}
            {!usersElements.length &&
                <div
                    className="flex flex-col p-7 bg-gray-800 border-gray-800 shadow-md hover:shodow-lg rounded-2xl">
                    <div className="flex items-center justify-between">
                        No users
                    </div>
                </div>
            }
        </div>
    );
}



