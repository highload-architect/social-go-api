import {useState} from "react";
import axiosInstance from "~/utils/axiosInstance";
import {useAuth} from "~/utils/authContext";

interface Props {
    fetchPosts: () => Promise<void>;
}

export default function PostCreate({fetchPosts}: Props) {
    const [postText, setPostText] = useState('');
    const [error, setError] = useState<string | null>(null);
    const [isLoading, setIsLoading] = useState(false);

    const {token} = useAuth();

    const handlePostCreate = async () => {
        try {
            setIsLoading(true);
            setError(null);

            // Отправка запроса на сервер для создания поста
            await axiosInstance.post(`/post/`, {
                    text: postText,
                },
                {
                    headers: {
                        Authorization: `Bearer ${token}`,
                    },
                });

            void fetchPosts();
            setPostText('');
        } catch (error: any) {
            console.error('Failed to create post:', error.message);
            setError('Failed to create post. Please try again later.');
        } finally {
            setIsLoading(false);
        }
    };

    return (
        <div>
            <div className="col-start-1 col-end-3 row-start-1 row-end-2">
                <h2 className="text-2xl w-full text-center p-2 h-12 pb-4">New post</h2>
            </div>

            <form className="flex flex-col">
                <textarea name="postText" placeholder="Post text" rows={6}
                          className="py-2 px-4 bg-gray-800 text-white rounded-md focus:outline-none mb-4 resize-none w-full col-span-2"
                          value={postText}
                          onChange={e => setPostText(e.target.value)}
                />
                <button type="button"
                        className="bg-blue-500 py-2 px-4 text-white rounded-md hover:bg-blue-600 focus:outline-none w-full"
                        disabled={isLoading}
                        onClick={handlePostCreate}
                >
                    {isLoading ? 'Creating Post...' : 'Create Post'}
                </button>
                {error && <p>Error: {error}</p>}
            </form>
        </div>
    );
}


