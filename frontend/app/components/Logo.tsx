import {Link} from "@remix-run/react";

export default function Logo() {
    return (
        <Link to='/' className="flex justify-start max-w-screen-lg w-full">
            <h1 className="w-full text-xl text-gray-200 font-bold leading-none p-6">Social APP</h1>
        </Link>
    );
}


