import {Post} from "~/interfaces/interfaces";

interface Props {
    error: string | null;
    posts: Post[];
    isLoading: boolean;
}

export default function PostFeed({posts, isLoading, error}: Props) {
    return (
        <div>
            <div>
                <h2 className="text-2xl w-full text-center p-2 h-12 pb-4">Feed</h2>
            </div>

            {isLoading && <p>Loading posts...</p>}
            {error && <p>Error: {error}</p>}
            {posts.length === 0 && !isLoading && <p>No posts available.</p>}
            {posts.length > 0 && (
                <div className="flex flex-col w-full">
                    {posts.map((post) => (
                        <div key={post.id} className="flex mb-4 w-full">
                            <div className="w-9 h-9 rounded-full flex items-center justify-center mr-2">
                                <img src="https://placehold.co/200x/ffa8e4/ffffff.svg?text=ʕ•́ᴥ•̀ʔ&font=Lato"
                                     alt="User Avatar"
                                     className="w-8 h-8 rounded-full"/>
                            </div>
                            <div className="flex flex-col bg-blue-900 rounded-lg p-3 gap-3 w-full">
                                <p className="text-gray-400 text-xs pb-2">
                                    <strong>{post.author_user_id}</strong>
                                    <br/>{post.created_at}
                                </p>
                                <p className="text-gray-200">{post.text}</p>
                            </div>
                        </div>
                    ))}
                </div>
            )
            }
        </div>
    );
}


