import {Dialog} from "~/interfaces/interfaces";
import {useAuth} from "~/utils/authContext";

interface Props {
    message: Dialog
}

export default function DialogMessage({message}: Props) {
    const {userId} = useAuth();

    if (userId === message.to_user_id) {
        return (
            <div className="flex mb-4 cursor-pointer">
                <div className="w-9 h-9 rounded-full flex items-center justify-center mr-2">
                    <img src="https://placehold.co/200x/ffa8e4/ffffff.svg?text=ʕ•́ᴥ•̀ʔ&font=Lato"
                         alt="User Avatar"
                         className="w-8 h-8 rounded-full"/>
                </div>
                <div className="flex max-w-96 bg-blue-900 rounded-lg p-3 gap-3">
                    <p className="text-gray-300">{message.text}</p>
                </div>
            </div>
        )
    }

    return (
        <div className="flex justify-end mb-4 cursor-pointer">
            <div className="flex max-w-96 bg-indigo-500 text-gray-300 rounded-lg p-3 gap-3">
                <p>{message.text}</p>
            </div>
            <div className="w-9 h-9 rounded-full flex items-center justify-center ml-2">
                <img src="https://placehold.co/200x/b7a8ff/ffffff.svg?text=ʕ•́ᴥ•̀ʔ&font=Lato"
                     alt="My Avatar"
                     className="w-8 h-8 rounded-full"/>
            </div>
        </div>
    );
}



