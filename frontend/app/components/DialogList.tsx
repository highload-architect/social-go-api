import DialogListElement from "~/components/DialogListElement";
import {User} from "~/interfaces/interfaces";

interface Props {
    searchResults: User[]
}

export default function DialogList({ searchResults }: Props) {
    const dialogsElements = searchResults.map((user) => <DialogListElement key={user.id} user={user}/>)

    return (
        <div className="flex flex-col gap-4">
            {dialogsElements}
        </div>
    );
}



