import {User} from "~/interfaces/interfaces";
import {Link} from "@remix-run/react";

interface Props {
    user: User
}

export default function DialogListElement({user}: Props) {
    const isOnline = Math.random() > 0.5;

    return (
        <Link to={`/dialogs/${user.id}`}
              className="flex flex-col p-4 bg-gray-800 border-gray-800 shadow-md hover:shodow-lg rounded-2xl cursor-pointer transition ease-in duration-500  transform hover:scale-105">
            <div className="flex items-center justify-between">
                <div className="flex items-center mr-auto">
                    <div className="inline-flex w-12 h-12"><img
                        src="https://tailwindcomponents.com/storage/avatars/njkIbPhyZCftc4g9XbMWwVsa7aGVPajYLRXhEeoo.jpg"
                        alt="aji" className=" relative p-1 w-12 h-12 object-cover rounded-2xl"/><span
                        className="absolute w-12 h-12 inline-flex border-2 rounded-2xl border-gray-600 opacity-75"></span>
                    </div>

                    <div className="flex flex-col ml-3 min-w-0">
                        <div
                            className="font-medium leading-none text-gray-100">{user.first_name} {user.second_name}</div>
                        <p className="text-sm text-gray-500 leading-none mt-1 truncate">{user.birthdate}</p>
                    </div>
                </div>
                <div className="flex flex-col ml-3 min-w-0">
                    <div className="flex">
                        <h5 className="flex items-center font-medium text-gray-300 mr-2">
                            {user.email}
                        </h5>
                        {isOnline &&
                            <svg xmlns="http://www.w3.org/2000/svg" className="h-6 w-6 text-green-400 ml-2"
                                 viewBox="0 0 20 20"
                                 fill="currentColor">
                                <path fillRule="evenodd"
                                      d="M10 18a8 8 0 100-16 8 8 0 000 16zm3.707-9.293a1 1 0 00-1.414-1.414L9 10.586 7.707 9.293a1 1 0 00-1.414 1.414l2 2a1 1 0 001.414 0l4-4z"
                                      clipRule="evenodd"/>
                            </svg>
                        }
                    </div>
                </div>
            </div>
        </Link>
    );
}



