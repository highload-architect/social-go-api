import axios from 'axios';

const axiosInstance = axios.create({
    baseURL: 'http://localhost:8080', // Замените на ваш базовый URL
    timeout: 5000, // Максимальное время ожидания ответа от сервера
    headers: {
        'Content-Type': 'application/json',
        // Добавьте любые другие заголовки, которые вам могут потребоваться
    },
});

export default axiosInstance;
