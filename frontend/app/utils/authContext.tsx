import React, {createContext, ReactNode, useContext, useEffect, useState} from 'react';

interface AuthContextProps {
    token: string | null;
    setToken: (token: string | null) => void;
    userId: string | null;
    setUserId: (userId: string | null) => void;
}

const AuthContext = createContext<AuthContextProps | undefined>(undefined);

interface AuthProviderProps {
    children: ReactNode;
}

export const AuthProvider: React.FC<AuthProviderProps> = ({children}) => {
    const storedToken = (typeof window !== 'undefined') && localStorage.getItem('token');
    const storedUserId = (typeof window !== 'undefined') && localStorage.getItem('userId');
    const [token, setToken] = useState<string | null>(storedToken || null);
    const [userId, setUserId] = useState<string | null>(storedUserId || null);

    useEffect(() => {
        if (token) localStorage.setItem('token', token);
        if (userId) localStorage.setItem('userId', userId);
    }, [token, userId]);

    return (
        <AuthContext.Provider value={{token, setToken, userId, setUserId}}>
            {children}
        </AuthContext.Provider>
    );
};

export const useAuth = () => {
    const context = useContext(AuthContext);
    if (!context) {
        throw new Error('useAuth must be used within an AuthProvider');
    }
    return context;
};
